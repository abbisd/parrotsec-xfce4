# Changelog 11/12/2021

### Added:

- gobuster
- msfpc
- usefull bookmarks
- created foxyproxy burp profile
- some wordlists can be found in /usr/share/dirb

### Deleted:

- xfburn
- xfce4-dict
- xfce4-sensors-plugin

### Bookmarks

- [Hack Tricks](https://book.hacktricks.xyz/)
- [NetSec](https://netsec.ws/)
- [Ippsec Search](https://ippsec.rocks)
- [Payload All The Things](https://github.com/swisskyrepo/PayloadsAllTheThings)
