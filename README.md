# parrotsec-xfce4

## Getting started

Depending on your operating system install docker using one of the guides from the official docs

[Docker Install Guides](https://docs.docker.com/engine/install/)

After the install check if it is working with 

```
docker -v
docker run --rm hello-world
```

If all looks good you can proceed to install sysbox that is officially available for:

- Ubuntu (18.04,20.04)
- Debian (10,11)
- Fedora (31,32,33,34)
- CentOS 8

Unofficially: 

- Arch (AUR package)

Please read the guides regarding sysbox from:

[sysbox git](https://github.com/nestybox/sysbox)

## Setting up 

Create or edit /etc/docker/daemon.json with the following config:

```json
{
   "userns-remap": "sysbox",
   "runtimes": {
       "sysbox-runc": {
          "path": "/usr/bin/sysbox-runc"
       }
   }
}
```

Do not forget to run sysbox after install

## Running the container

The bare minimum command needed to run the container is:

```sh
docker run --rm -ti  --runtime=sysbox-runc --shm-size=2g abaddonsd/parrotsec-xfce
```

Shm size is needed for browsers and other applications to work properly.

Once you are in the container run:

```sh
sudo service nxserver start
```

The password is: parrot (please change it)

If you need to run openvpn or something that needs tun device run like this:

```sh
docker run --rm -ti  --runtime=sysbox-runc --shm-size=2g --device=/dev/net/tun  --sysctl net.ipv6.conf.all.disable_ipv6=0 abaddonsd/parrotsec-xfce4
```

Also please read the CHANGELOG.
